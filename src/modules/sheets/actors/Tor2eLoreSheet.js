import {tor2eUtilities} from "../../utilities.js";
import {StatusEffects} from "../../effects/status-effects.js";

export default class Tor2eLoreSheet extends ActorSheet {
    /** @override */
    static get defaultOptions() {
        return mergeObject(super.defaultOptions, {
            classes: ["tor2e", "sheet", "actor"],
            width: 450,
            height: 525,
            template: `${CONFIG.tor2e.properties.rootpath}/templates/sheets/actors/lorecharacter-sheet.hbs`
        });
    }

    getData() {
        const baseData = super.getData();
        baseData.config = CONFIG.tor2e;
        baseData.backgroundImages = CONFIG.tor2e.backgroundImages["lore"];

        baseData.effects = {
            "weary": this.actor.buildStatusEffectById(StatusEffects.WEARY),
            "wounded": this.actor.buildStatusEffectById(StatusEffects.WOUNDED),
            "poisoned": this.actor.buildStatusEffectById(StatusEffects.POISONED),
        }

        return {
            owner: this.actor.isOwner,
            data: baseData.actor.data.data,
            actor: baseData.actor,
            config: CONFIG.tor2e,
            backgroundImages: CONFIG.tor2e.backgroundImages["lore"],
            effects: {
                "weary": this.actor.buildStatusEffectById(StatusEffects.WEARY),
                "wounded": this.actor.buildStatusEffectById(StatusEffects.WOUNDED),
                "poisoned": this.actor.buildStatusEffectById(StatusEffects.POISONED),
            }
        };
    }

    activateListeners(html) {
        super.activateListeners(html);

        /*
            code pattern
            html.find(cssSelector).event(this._someCallBack.bind(this));
         */
        html.find(".toggleTor2eEffect").click(tor2eUtilities.eventsProcessing.onToggleEffect.bind(this));
        html.find(".toggle").click(tor2eUtilities.eventsProcessing.onToggle.bind(this));
        html.find(".editor-toggle").click(tor2eUtilities.eventsProcessing.onEditorToggle.bind(this));

    }

}